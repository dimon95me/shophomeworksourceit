package examplesoureit.shopsourceithomework;

import java.util.ArrayList;
import java.util.List;

public final class Generator {

    public Generator() {
    }

    public static List<Product> generate(){

        List<Product> productList = new ArrayList<>();
        productList.add(new Product("Milk", 500));
        productList.add(new Product("Meat",300));
        productList.add(new Product("Cheese",80));
        productList.add(new Product("Flour",1000));
        productList.add(new Product("Eggs",1500));

        return productList;
    }

}
