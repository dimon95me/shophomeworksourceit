package examplesoureit.shopsourceithomework;

import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.list_view)
    ListView listView;

    @BindView(R.id.action_work)
    Button actionWork;

    List<Product> productList;

    ProductListAdapter productListAdapter;

    Queue<Product> waitList = new ArrayDeque<>();

    Random random = new Random();

    boolean onStart = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        productList = Generator.generate();

        productListAdapter = new ProductListAdapter(this, productList);
        listView.setAdapter(productListAdapter);

/*
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    buying();
                }
            }
        });
*/

//listView.deferNotifyDataSetChanged();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


//                for (int i = 0; i < 5; i++) {

                int randomProduct = random.nextInt(productList.size());
                Product product = productList.get(randomProduct);

                if (onStart) {

                    if (waitList!=null){
                        onRestoreWorkShop();
                    }
                    if (product.getCount() < 0) {
                        product.setCount(1000);
                    }
                    product.setCount(product.getCount() - random.nextInt(50));

                    productList.set(randomProduct, product);
                    Log.d("my_tag", "productList was changed");

                    if (productList == null) {
                        productListAdapter = new ProductListAdapter(MainActivity.this, productList);
                        listView.setAdapter(productListAdapter);
                    } else {
                        productListAdapter.notifyDataSetChanged();
                    }
                } else{
                    Log.d("my_tag", "onStart = " + onStart);
                    onWaiting(product);
                }

                handler.postDelayed(this, 1000);
//                }
            }
        }, 1000);


    }

    @OnClick(R.id.action_work)
    public void onActionworkClick(){
        if (onStart){
            onStart = false;
            actionWork.setText("Start");

        } else{
            onStart = true;
            actionWork.setText("Stop");
        }
    }

    private void onWaiting(Product product) {

        boolean verifyWaitList = true;
        for (Product product1 : productList) {
            if (product.getName() == product1.getName()) {
                verifyWaitList = false;
            }
        }
        if (verifyWaitList) {
            product.setCount(random.nextInt(50));

        } else {
            product.setCount(product.getCount() + random.nextInt(50));

        }

    }

    private void onRestoreWorkShop(){
        for (Product waitProduct : waitList) {
            for (Product realProduct : productList) {
                if (waitProduct.getName() == realProduct.getName()){
                    realProduct.setCount(realProduct.getCount() - waitProduct.getCount());
                }
            }
        }
    }




}
